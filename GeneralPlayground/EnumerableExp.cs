﻿using System;
using System.Collections;
using System.Threading;

namespace GeneralPlayground
{
    public class EnumerableExp
    {
        public void Execute()
        {
            var myList = new MyReverseList();
            myList.Add(1);
            myList.Add(2);
            myList.Add(3);

            foreach (var item in myList)
            {
                Console.WriteLine(item);
            }

            var myLazyEnumerable = new MyLazyLoadEnumerable();
            var count = 0;
            foreach (var item in myLazyEnumerable)
            {
                Console.WriteLine(item);
                if (++count >= 5) break;
            }
        }
    }

    public class MyReverseList : IEnumerable
    {
        public readonly object[] Data = new object[10];
        public int Count = 0;

        public void Add(object item)
        {
            Data[Count] = item;
            Count++;
        }

        public IEnumerator GetEnumerator()
        {
            return new MyReverseEnumerator(this);
        }
    }

    public class MyReverseEnumerator : IEnumerator
    {
        private readonly MyReverseList enumerable;
        private int index;

        public MyReverseEnumerator(MyReverseList enumerable)
        {
            this.enumerable = enumerable;
            Reset();
        }

        public object Current => enumerable.Data[index];

        public bool MoveNext()
        {
            if (index > 0)
            {
                index--;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            index = enumerable.Count;
        }
    }

    public class MyLazyLoadEnumerable : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            return new MyLazyLoadEnumerator();
        }
    }

    public class MyLazyLoadEnumerator : IEnumerator
    {
        private object currentData = null;

        public object Current => currentData;

        public bool MoveNext()
        {
            currentData = FetchNextData();
            return true;
        }

        // Simulate undetermine lazy loading
        private object FetchNextData()
        {
            Thread.Sleep(1);
            return DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }

        public void Reset()
        {
            currentData = null;
        }
    }
}
