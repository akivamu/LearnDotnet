﻿using System;

namespace GeneralPlayground
{
    class Program
    {
        static void Main(string[] args)
        {
            new EnumerableExp().Execute();

            Console.ReadLine();
        }
    }
}
