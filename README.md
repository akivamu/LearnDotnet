# IQueryable vs IEnumerable

http://ivanitskyi.blogspot.com/2013/04/entity-framework-iqueryable-vs-ienumerable.html?m=1


https://stackoverflow.com/questions/43419228/iqueryable-vs-ienumerable-is-iqueryable-always-better-and-faster

https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/ef/language-reference/supported-and-unsupported-linq-methods-linq-to-entities

Look at `LINQExperiment/EnumerableQueryableExp.cs`

## Test 1: method `Count()`

We can see the call to `Count()` will trigger to build SQL query in both cases.

For the case of `Enumerable`: LINQ generates the query to fetch all rows to memory, then applying `Count()` to the result set in memory.  
That why we see the SQL:  
```
SELECT "e"."Id", "e"."IsActive", "e"."Name"
FROM "Employee" AS "e"
```

For the case of `Queryable`: LINQ generates the query in smarter way (add `COUNT(*)` to the query):  
```
SELECT COUNT(*)
FROM "Employee" AS "e"
```
Clearly this increases performance as compared to `Enumerable`.  
But [not all methods are supported in this smart ways](https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/ef/language-reference/supported-and-unsupported-linq-methods-linq-to-entities).

## Test 2: method `CountWithQuery()`

Here we add some filter: `Where(e => e.IsActive)`

For the case of `Enumerable`: the SQL query isn't changed. It fetchs all rows to memory first, then do the filter on in-memory result set, then count.

For the case of `Queryable`:
```
SELECT COUNT(*)
FROM "Employee" AS "e"
WHERE "e"."IsActive" = 1
```
Here we can see LINQ on `Queryable` is very smart, it combines the filter and count into a single query, which is a huge performance boost.

One important note is that we can pass a `Queryable` around, adding condition, filter... and then LINQ will compose the final SQL query in optimal way.

In contrast, the first time we cast a LINQ to `Enumerable` instance, the SQL query is composed. Further actions on that instance won't change the SQL query, it just perform on the in-memory result set from that query.

## Test 3: method `CountWithQueryMixed()`

Same logic as test 2, but notice the query for case of `Enumerable` is changed:
```
SELECT "e"."Id", "e"."IsActive", "e"."Name"
FROM "Employee" AS "e"
WHERE "e"."IsActive" = 1
```
We can see the clause `WHERE "e"."IsActive" = 1` is added to SQL query.

Why? Look at the code line:
```
IEnumerable<Employee> enumerable = dbContext.Employee.Where(e => e.IsActive);
```
This is the difference: the `Where` is applied to a `Queryable`, not `Enumerable`. That means `dbContext.Employee` is a `Queryable`, we not cast to `Enumerable` yet.  
The `Where` will returns a `Queryable`, but then we cast to `Enumerable`. And that's why the later `Count()` will be applied to `Enumerable`, and not be optimized by LINQ.