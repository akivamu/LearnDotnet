﻿using LINQExperiment.CustomFilter;
using LINQExperiment.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LINQExperiment
{

    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>()
        {
            return (Expression<Func<T, bool>>)(input => true);
        }

        public static Expression<Func<T, bool>> False<T>()
        {
            return (Expression<Func<T, bool>>)(input => false);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            InvocationExpression invocationExpression = Expression.Invoke((Expression)expression2, expression1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>((Expression)Expression.OrElse(expression1.Body, (Expression)invocationExpression), (IEnumerable<ParameterExpression>)expression1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            InvocationExpression invocationExpression = Expression.Invoke((Expression)expression2, expression1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>((Expression)Expression.AndAlso(expression1.Body, (Expression)invocationExpression), (IEnumerable<ParameterExpression>)expression1.Parameters);
        }
    }

    public class CustomFilterExp
    {
        private readonly MyDbContext dbContext;
        private readonly ILogger<CustomFilterExp> logger;

        public CustomFilterExp()
        {
            this.dbContext = DIContainer.CreateDbContext(DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString());

            this.logger = DIContainer.GetLoggerFactory().CreateLogger<CustomFilterExp>();
        }

        public void Execute()
        {
            InsertSampleRows();


            var workItemFieldFilter = new List<FieldValueCondition>
            {
                new FieldValueCondition
                {
                    FieldName = "ProjectId",
                    Value = "1"
                }
            };
            var filter = new WorkItemFilter
            {
                EmployeeId = null,
                IncludeUnassigned = true,
                WorkItemConditions = workItemFieldFilter

            };

            logger.LogInformation($"---- Begin filter");
            var result = Query(filter).ToList();

            logger.LogInformation($"Total: {result.Count}");
        }

        private IQueryable<WorkItem> Query(WorkItemFilter filter)
        {
            var workItems = dbContext.WorkItem.AsQueryable();

            if (filter.WorkItemConditions != null)
            {
                workItems = FilterWorkItem(workItems, filter.WorkItemConditions); ;
            }

            var filteredProjectsByAssignment = FilterProject(dbContext.Project, filter.EmployeeId, filter.IncludeUnassigned);
            workItems = workItems.Where(w => filteredProjectsByAssignment.Any(project => project.Id == w.ProjectId));

            return workItems;
        }

        private Expression<Func<WorkItem, bool>> BuildWorkItemFieldCondition(string fieldName, object value)
        {
            Expression<Func<WorkItem, bool>> expression = workItem => true;
            switch (fieldName)
            {
                case nameof(WorkItem.ProjectId):
                    {
                        int id = int.Parse((string)value);
                        expression = workItem => workItem.ProjectId == id;
                        break;
                    }
            }

            return expression;
        }

        private IQueryable<WorkItem> FilterWorkItem(IQueryable<WorkItem> workItems, List<FieldValueCondition> fieldValueConditions)
        {
            Expression<Func<WorkItem, bool>> finalExpression = workItem => true;

            foreach (var filter in fieldValueConditions)
            {
                finalExpression = finalExpression.And(BuildWorkItemFieldCondition(filter.FieldName, filter.Value));
            }

            var resultQuery = workItems.Where(finalExpression);

            return resultQuery;
        }

        private IQueryable<Project> FilterProject(IQueryable<Project> projects, int? employeeIdOption, bool includeUnassigned)
        {
            Expression<Func<Project, bool>> finalExpression = project => false;
            if (includeUnassigned)
            {
                Expression<Func<Project, bool>> unassignedExpression = project => !dbContext.ProjectAssignment.Any(assignment => assignment.ProjectId == project.Id);
                finalExpression = finalExpression.Or(unassignedExpression);
            }

            if (employeeIdOption.HasValue)
            {
                Expression<Func<Project, bool>> assignedToSpecificEmployee = project => dbContext.ProjectAssignment.Any(assignment => assignment.ProjectId == project.Id && assignment.EmployeeId == employeeIdOption.Value);
                finalExpression = finalExpression.Or(assignedToSpecificEmployee);
            }
            else
            {
                Expression<Func<Project, bool>> assignedToAnyEmployee = project => dbContext.ProjectAssignment.Any(assignment => assignment.ProjectId == project.Id);
                finalExpression = finalExpression.Or(assignedToAnyEmployee);
            }

            var resultQuery = projects.Where(finalExpression);

            return resultQuery;
        }


        private void InsertSampleRows()
        {
            dbContext.Employee.Add(new Employee
            {
                Name = "Alice",
                IsActive = true
            });
            dbContext.Employee.Add(new Employee
            {
                Name = "Bob",
                IsActive = false
            });
            dbContext.Employee.Add(new Employee
            {
                Name = "Carrie",
                IsActive = true
            });

            dbContext.Project.Add(new Project
            {
                Name = "Project A"
            });
            dbContext.Project.Add(new Project
            {
                Name = "Project B"
            });

            // Project A has assignment
            dbContext.ProjectAssignment.Add(new ProjectAssignment
            {
                ProjectId = 1,
                EmployeeId = 1,
            });

            dbContext.WorkItem.Add(new WorkItem
            {
                ProjectId = 1,
                Data = "Sample Data",
            });

            // Add unassigned item
            dbContext.WorkItem.Add(new WorkItem
            {
                ProjectId = 2,
                Data = "Sample Data",
            });

            dbContext.SaveChanges();
        }
    }
}
