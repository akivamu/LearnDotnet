﻿using System.Collections.Generic;

namespace LINQExperiment.CustomFilter
{
    public class WorkItemFilter
    {
        // Project assignment
        public int? EmployeeId { get; set; }
        public bool IncludeUnassigned { get; set; }

        // WorkItem fields
        public List<FieldValueCondition> WorkItemConditions { get; set; }
    }

    public class FieldValueCondition
    {
        public string FieldName { get; set; }
        public string Value { get; set; }
    }
}
