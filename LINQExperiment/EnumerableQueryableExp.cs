﻿using LINQExperiment.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQExperiment
{
    public class EnumerableQueryableExp
    {
        private readonly MyDbContext dbContext;
        private readonly ILogger<EnumerableQueryableExp> logger;

        public EnumerableQueryableExp()
        {
            this.dbContext = DIContainer.CreateDbContext(DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString());

            this.logger = DIContainer.GetLoggerFactory().CreateLogger<EnumerableQueryableExp>();
        }

        public void Execute()
        {
            InsertSampleRows();

            Count();
            CountWithQuery();
            CountWithQueryMixed();
        }

        private void Count()
        {
            /*
                SELECT "e"."Id", "e"."IsActive", "e"."Name"
                FROM "Employee" AS "e"
             */
            IEnumerable<Employee> enumerable = dbContext.Employee;
            logger.LogInformation($"---- Enumerable - Total: {enumerable.Count()}");

            /*
                SELECT COUNT(*)
                FROM "Employee" AS "e"
             */
            IQueryable<Employee> queryable = dbContext.Employee;
            logger.LogInformation($"---- Queryable - Total: {queryable.Count()}");
        }

        private void CountWithQuery()
        {
            /*
                SELECT "e"."Id", "e"."IsActive", "e"."Name"
                FROM "Employee" AS "e"
             */
            IEnumerable<Employee> enumerable = dbContext.Employee;
            enumerable = enumerable.Where(e => e.IsActive);
            logger.LogInformation($"---- Enumerable - Total: {enumerable.Count()}");

            /*
                SELECT COUNT(*)
                FROM "Employee" AS "e"
                WHERE "e"."IsActive" = 1
             */
            IQueryable<Employee> queryable = dbContext.Employee;
            queryable = queryable.Where(e => e.IsActive);
            logger.LogInformation($"---- Queryable - Total: {queryable.Count()}");
        }

        private void CountWithQueryMixed()
        {
            /*
                SELECT "e"."Id", "e"."IsActive", "e"."Name"
                FROM "Employee" AS "e"
                WHERE "e"."IsActive" = 1
             */
            IEnumerable<Employee> enumerable = dbContext.Employee.Where(e => e.IsActive);
            logger.LogInformation($"---- Enumerable - Total: {enumerable.Count()}");

            /*
                SELECT COUNT(*)
                FROM "Employee" AS "e"
                WHERE "e"."IsActive" = 1
             */
            IQueryable<Employee> queryable = dbContext.Employee.Where(e => e.IsActive);
            logger.LogInformation($"---- Queryable - Total: {queryable.Count()}");
        }

        private void InsertSampleRows()
        {
            dbContext.Employee.Add(new Employee
            {
                Name = "Alice",
                IsActive = true
            });
            dbContext.Employee.Add(new Employee
            {
                Name = "Bob",
                IsActive = false
            });
            dbContext.Employee.Add(new Employee
            {
                Name = "Carrie",
                IsActive = true
            });
            dbContext.SaveChanges();
        }
    }
}
