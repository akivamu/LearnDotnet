﻿using LINQExperiment.Models;
using Microsoft.EntityFrameworkCore;

namespace LINQExperiment
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {

        }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<ProjectAssignment> ProjectAssignment { get; set; }
        public DbSet<WorkItem> WorkItem { get; set; }

    }
}
