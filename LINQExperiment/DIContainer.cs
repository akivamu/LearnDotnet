﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LINQExperiment
{
    public static class DIContainer
    {
        private static ILoggerFactory loggerFactory = new LoggerFactory()
                .AddConsole();

        public static MyDbContext CreateDbContext(string dbFileName = "test.db")
        {
            var optionsBuilder = new DbContextOptionsBuilder<MyDbContext>();
            optionsBuilder.UseSqlite($"Data Source={dbFileName}");
            optionsBuilder.UseLoggerFactory(loggerFactory);

            var dbContext = new MyDbContext(optionsBuilder.Options);
            dbContext.Database.EnsureCreated();

            return dbContext;
        }

        public static ILoggerFactory GetLoggerFactory()
        {
            return loggerFactory;
        }
    }
}
