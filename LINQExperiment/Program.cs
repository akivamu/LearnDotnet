﻿using System;

namespace LINQExperiment
{
    class Program
    {
        static void Main(string[] args)
        {
            new CustomFilterExp().Execute();

            Console.ReadLine();
        }
    }
}
