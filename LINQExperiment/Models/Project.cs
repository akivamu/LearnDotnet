﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQExperiment.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
