﻿namespace LINQExperiment.Models
{
    public class WorkItem
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Data { get; set; }
    }
}
